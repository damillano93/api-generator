# GENERADOR DE APIS

_aplicacion para la generacion de API RESTFULL con MongoDB Express Nodejs_
_el aplicativo usa el archivo model.json para generar todos los archivos correpondientes y generar una api_





### Pre-requisitos 📋

_nodejs_
_nodemon_
_mongoDB_


### Instalación 🔧

_Clonar repositorio_

```
git clone https://gitlab.com/damillano93/api-generator
```

_entrar carpeta_

```
cd api-generator 
```
_generar formulario_

```
node generate_api.js
```
_entrar en la carpeta generada_

```
cd example
```
_instalar modulos_

```
npm install 
```
_ejecutar proyecto_

```
nodemon app.js 
```



## CLIENTE ⚙️

_para la generacion del cliente debe usar el generador de api [api-generator](https://gitlab.com/damillano93/client-generator)_



## Autor✒️



* **David MIllan** - *Trabajo Inicial* - [damillano93](https://gitlab.com/damillano93)



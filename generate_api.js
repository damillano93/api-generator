// imortar bibliotecas
const fs = require('fs');
const Handlebars = require('handlebars');
//leer JSON de los datos
var obj = JSON.parse(fs.readFileSync('model.json', 'utf8'));
// creacion de la carpeta de la api 
var dir = './' + obj.api_name;
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}
// creacion de la carpeta models 
var models = './' + dir + '/models';
if (!fs.existsSync(models)) {
    fs.mkdirSync(models);
}
// creacion de la carpeta routes 
var routes = './' + dir + '/routes';
if (!fs.existsSync(routes)) {
    fs.mkdirSync(routes);
}
// creacion de la carpeta controllers 
var controllers = './' + dir + '/controllers';
if (!fs.existsSync(controllers)) {
    fs.mkdirSync(controllers);
}
//leer template del modelo 
var model = fs.readFileSync('./template/model_template.txt', 'utf8')
//leer template del controller 
var controller = fs.readFileSync('./template/controller_template.txt', 'utf8')
//leer template del route 
var route = fs.readFileSync('./template/route_template.txt', 'utf8')
//leer template del app
var appi = fs.readFileSync('./template/app_template.txt', 'utf8')
//leer template del package
var package = fs.readFileSync('./template/package_template.txt', 'utf8')
//lee numero de tablas de la api
var len_obj = obj.collections.length;
// crea nombre de la tabla
var import_router = ''
var create_route = ''

for (var j = 0; j < len_obj; j++) {
    var relation = ''
    var route_relation = ''
    var Unombre = jsUcfirst(obj.collections[j].name)
    var nombre = obj.collections[j].name
    // genera el template del modelo 
    const template_model = Handlebars.compile(model, { noEscape: true });
    // agrega los campos de la tabla
    var len_campos = obj.collections[j].fields.length
    var campos = ''

    for (var i = 0; i < len_campos; i++) {
        if (obj.collections[j].fields[i].type == "Relation") {

            relation+= '//funcion add '+obj.collections[j].fields[i].name+''+'\n'+
            'exports.'+Unombre+'_add_'+obj.collections[j].fields[i].name+' \= function (req, res) {'+'\n'+
            ''+Unombre+'.findByIdAndUpdate(req.params.id, { $push: { '+obj.collections[j].fields[i].name+': req.body.'+obj.collections[j].fields[i].name+' } }, { new: true }, function (err, '+Unombre+') {'+'\n'+
            'if (err) {'+'\n'+
            'res.send({ status: \"Err\", error: err })'+'\n'+
            'return next(err);'+'\n'+
            '}'+'\n'+
            'res.send({ status: \"updated\", '+Unombre+': '+Unombre+' });'+'\n'+
            '});'+'\n'+
            '};'+'\n';
            route_relation='// PUT '+obj.collections[j].fields[i].name+' '+'\n'+
            'router.put(\'/'+obj.collections[j].fields[i].name+'/:id\','+nombre+'_controller.'+Unombre+'_add_'+obj.collections[j].fields[i].name+');'+'\n'
            if (obj.collections[j].fields[i].array == true) {
                campos += obj.collections[j].fields[i].name + ':[ {type:' + 'Schema.Types.ObjectId' + ', ref: \'' + jsUcfirst(obj.collections[j].fields[i].name) + '\' , autopopulate: true }], \n'
            } else {
                campos += obj.collections[j].fields[i].name + ': {type:' + 'Schema.Types.ObjectId' + ', ref: \'' + jsUcfirst(obj.collections[j].fields[i].name) + '\' , autopopulate: true }, \n'
            }

        } else {
            campos += obj.collections[j].fields[i].name + ': {type:' + obj.collections[j].fields[i].type + ', required:' + obj.collections[j].fields[i].required + '}, \n'
        }



    }
    // agrega los campos del template al modelo
    const contents_model = template_model({ name: Unombre, fields: unescape(campos) });
    //genera el archivo del modelo
    fs.writeFile('./' + dir + '/models/' + nombre + '.js', contents_model, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });

    // genera el template del controller 
    const template_controller = Handlebars.compile(controller, { noEscape: true });
    // agrega los campos al template al controller
    const contents_controller = template_controller({ name: nombre, Uname: Unombre, relation: unescape(relation) });
    //genera el archivo del controller
    fs.writeFile('./' + dir + '/controllers/' + nombre + '.js', contents_controller, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });

    // genera el template del route 
    const template_route = Handlebars.compile(route , { noEscape: true });
    // agrega los campos al template al route
    const contents_route = template_route({ name: nombre, route_relation:unescape(route_relation) });
    //genera el archivo del route
    fs.writeFile('./' + dir + '/routes/' + nombre + '.js', contents_route, err => {
        if (err) {
            return console.error(`Error: ${err.message}.`);
        }


    });

    import_router += '// importar router ' + nombre + '\n' +
        'var ' + nombre + ' = require(\'./routes/' + nombre + '\');' + '\n'


    create_route += '//creacion ruta /' + nombre + '\n' +
        'app.use(\'/' + nombre + '\', ' + nombre + ');' + '\n'
}

// genera el template del app 
const template_app = Handlebars.compile(appi, { noEscape: true });
// agrega los campos al template al route
const contents_app = template_app({ import_router: unescape(import_router), create_route: create_route, mongo_url: obj.db_url, port: obj.port });
//genera el archivo app
fs.writeFile('./' + dir + '/app.js', contents_app, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }


});
//copia controller status
fs.copyFile('./template/status_controller.txt', './' + dir + '/controllers/status.js', (err) => {
    if (err) throw err;
    //copia route status   
});
fs.copyFile('./template/status_route.txt', './' + dir + '/routes/status.js', (err) => {
    if (err) throw err;

});
if (obj.Dockerfile == true) {
    //genera dockerfile 
    fs.copyFile('./template/Dockerfile_template.txt', './' + dir + '/Dockerfile', (err) => {
        if (err) throw err;

    });
}
if (obj.CI == true) {
    //genera dockerfile 
    fs.copyFile('./template/drone_template.txt', './' + dir + '/.drone.yml', (err) => {
        if (err) throw err;

    });
}
// genera el template del package 
const template_package = Handlebars.compile(package);
// agrega los campos al template al route
const contents_package = template_package({ api_name: obj.api_name });
//genera el archivo app
fs.writeFile('./' + dir + '/package.json', contents_package, err => {
    if (err) {
        return console.error(`Error: ${err.message}.`);
    }


});

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}